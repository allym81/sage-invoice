angular
    .module('sageApp')
    .service('CustomerService', ['$http', function($http) {
        this.getCustomers = function () {
            return $http.get('/customer').then(function (result) {
                return result.data;
            }, function (error) {
                return [];
            })
        };
    }]);
/**
 * Created by Alex on 4/18/2017.
 */
