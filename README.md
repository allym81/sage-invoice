# README #
# SAGE INVOICE #

### What is this repository for? ###

This is a SPA demo for managing invoices from different storage options. I've implemented Database (Sqlite) and JSON as storage options.
The code allows to very easily to add more storage options like CSV, webservices, etc.

### Description ###
I've created a demo application using .NET 4.6 (REST webservice using NancyFx) and AngularJs. The Solution was developed in Visual Studio 2015.
The solutions has 3rd party dependencies which can be resolved using Nuget.

### Architecture ###
The solution runs in a self-hosted application (OWIN). I wanted not to have IIS as dependency for showing the application  
Because it's a Single Page Application solution the architecture comprises 2 main layers:
1. UI build using JavaScript/Angular JS
2. Back-end API build using C# 

The back-end is developed on top of NancyFx which is a lightweight web framework that allows to quickly build a REST API
Also it include a very light DI container. See here [NancyFX](https://github.com/NancyFx/Nancy) for more information.

### How do I get set up? ###

In the */dist* folder start the *Sage.HostService.exe*
Note that the application must me run as administrator. 
Then open a browser and go to [http://localhost:5555/Content/index.html](http://localhost:5555/Content/index.html#/)

The *Sage.HostService.exe*  is the host service that serves the API and the static content. It will start a http server that by default has the ### http://localhost:5555 ###
In the *Sage.HostService.exe.config* you can configure the datasource (json or database), the json files location and the database location.
Before using the json as data source please configure the key:
```
#!xml

    <add key="fileStorageLocation" value="D:\Dev\sage-invoice\src\Sage.HostService\bin\Debug\Data"/>
```