namespace Sage.Data.Adapters
{
    public interface IFileStorage
    {
        string GetFileContent();
        void WriteFile(string data);
    }
}