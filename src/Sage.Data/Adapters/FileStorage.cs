﻿using System.IO;
using Sage.Data.Providers;

namespace Sage.Data.Adapters
{
    public class FileStorage : IFileStorage
    {
        private readonly string _path;

        public FileStorage(string path)
        {
            _path = path;
        }

        public string GetFileContent()
        {
            return File.ReadAllText(_path);
        }

        public void WriteFile(string data)
        {
            File.WriteAllText(_path, data);
        }
    }
}