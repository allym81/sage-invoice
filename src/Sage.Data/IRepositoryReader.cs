using System.Collections.Generic;

namespace Sage.Data
{
    public interface IRepositoryReader<out T> where T: class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
    }
}