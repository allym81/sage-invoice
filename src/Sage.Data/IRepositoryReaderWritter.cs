namespace Sage.Data
{
    public interface IRepositoryReaderWritter<T> : IRepositoryReader<T>, IRepositoryWriter<T> where T : class
    {
        
    }
}