﻿namespace Sage.Data
{
    public interface IRepositoryWriter<in T> where T : class
    {
        void Insert(T model);
        void Update(T model);
    }
}
