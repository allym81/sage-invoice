﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using Sage.Data.Connection;
using Sage.Entites;

namespace Sage.Data.Providers
{
    public class CustomerDatabaseRepository : IRepositoryReader<Customer>
    {
        private readonly IDbConnectionFactory _connectionFactory;
        public CustomerDatabaseRepository(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IEnumerable<Customer> GetAll()
        {
            const string sql = "select * from Customer";
            using (var conn = _connectionFactory.CreateConnection())
            {
                conn.Open();
                return conn.Query<Customer>(sql);
            }
        }

        public Customer Get(int id)
        {
            const string sql = "select * from Customer where Id = @customerId";
            using (var conn = _connectionFactory.CreateConnection())
            {
                conn.Open();
                return conn.Query<Customer>(sql, new {customerId = id}).FirstOrDefault();
            }
        }
    }
}