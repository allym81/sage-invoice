﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Sage.Data.Factories;
using Sage.Entites;

namespace Sage.Data.Providers
{
    public class InvoiceJsonRepository : IRepositoryReaderWritter<Invoice>
    {
        private readonly IFileStorageFactory _fileStorageFactory;

        public InvoiceJsonRepository(IFileStorageFactory fileStorageFactory)
        {
            _fileStorageFactory = fileStorageFactory;
        }
        public IEnumerable<Invoice> GetAll()
        {
            var fileStorage = _fileStorageFactory.GetInstance("invoices");
            if (fileStorage == null)
            {
                throw new ArgumentException("Could not get the file storage obj. don't know why");
            }
            var json = fileStorage.GetFileContent();
            var customersList = JsonConvert.DeserializeObject<IEnumerable<Invoice>>(json);
            return customersList;
        }

        public Invoice Get(int invoiceNumber)
        {
            var customerList = GetAll();
            return customerList.FirstOrDefault(c => c.Number.Equals(invoiceNumber));
        }

        public void Insert(Invoice model)
        {
            var list = GetAll().ToList();
            list.Add(model);
            var json = JsonConvert.SerializeObject(list);
            var fileStorage = _fileStorageFactory.GetInstance("invoices");
            if (fileStorage == null)
            {
                throw new ArgumentException("Could not get the file storage obj. don't know why");
            }
            fileStorage.WriteFile(json);
        }

        public void Update(Invoice model)
        {
            var list = GetAll().ToList();

            list.Where(i => i.Number.Equals(model.Number)).ToList()
                .ForEach(s => s.Status = model.Status);

            var json = JsonConvert.SerializeObject(list);
            var fileStorage = _fileStorageFactory.GetInstance("invoices");
            if (fileStorage == null)
            {
                throw new ArgumentException("Could not get the file storage obj. don't know why");
            }
            fileStorage.WriteFile(json);
        }
    }
}

