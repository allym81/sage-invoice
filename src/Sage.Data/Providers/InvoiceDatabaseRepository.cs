using System.Collections.Generic;
using System.Linq;
using Dapper;
using Sage.Data.Connection;
using Sage.Entites;

namespace Sage.Data.Providers
{
    public class InvoiceDatabaseRepository : IRepositoryReaderWritter<Invoice>
    {
        private readonly IDbConnectionFactory _connectionFactory;

        public InvoiceDatabaseRepository(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IEnumerable<Invoice> GetAll()
        {
            const string sql = "select * from Invoices join Customer on Invoices.CustomerId = Customer.Id";
            using (var conn = _connectionFactory.CreateConnection())
            {
                conn.Open();

                return conn.Query<Invoice, Customer, Invoice>(sql, (invoice, customer) =>
                {
                    invoice.Customer = customer;
                    return invoice;
                });
            }
        }

        public Invoice Get(int invoiceNumber)
        {
            const string sql = "select * from Invoices join Customer on Invoices.CustomerId = Customer.Id where Invoices.Number = @invoiceNumber";
            using (var conn = _connectionFactory.CreateConnection())
            {
                conn.Open();
                return conn.Query<Invoice, Customer, Invoice>(sql, (invoice, customer) =>
                {
                    invoice.Customer = customer;
                    return invoice;
                }, new {invoiceNumber }).FirstOrDefault();
            }
        }

        public void Insert(Invoice model)
        {
            const string sql = "insert into Invoices (Number, Date, CustomerId, Description, Amount, Status) values (@Number, @Date, @CustomerId, @Description, @Amount, @Status)";
            using (var conn = _connectionFactory.CreateConnection())
            {
                conn.Open();
                conn.QueryFirstOrDefault(sql, model);
            }
        }

        public void Update(Invoice model)
        {
            const string sql = "update Invoices Set Status = @status where Number = @number";
            using (var conn = _connectionFactory.CreateConnection())
            {
                conn.Open();
                conn.Execute(sql, new {status = model.Status, number = model.Number});
            }
        }
    }
}