﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Sage.Data.Factories;
using Sage.Entites;

namespace Sage.Data.Providers
{
    public class CustomerJsonRepository : IRepositoryReader<Customer>
    {
        private readonly IFileStorageFactory _fileStorageFactory;

        public CustomerJsonRepository(IFileStorageFactory fileStorageFactory)
        {
            _fileStorageFactory = fileStorageFactory;
        }
        public IEnumerable<Customer> GetAll()
        {
            var fileStorage = _fileStorageFactory.GetInstance("customers");
            var json = fileStorage.GetFileContent();
            var customersList = JsonConvert.DeserializeObject<IEnumerable<Customer>>(json);
            return customersList;
        }

        public Customer Get(int id)
        {
            var customerList = GetAll();
            return customerList.FirstOrDefault(c => c.Id.Equals(id));
        }


    }
}

