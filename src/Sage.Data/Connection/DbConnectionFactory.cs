﻿using System;
using System.Data;
using System.Data.SQLite;
using Dapper;
using Sage.Common;

namespace Sage.Data.Connection
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly ISageConfiguration _sageConfiguration;

        public DbConnectionFactory(ISageConfiguration sageConfiguration)
        {
            _sageConfiguration = sageConfiguration;
        }
        public IDbConnection CreateConnection()
        {
            //var sqLiteFile = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\sage-data.sqlite";
            var sqLiteFile = _sageConfiguration.GetConfigValue("databaseConnection");
            return new SQLiteConnection($"Data Source={sqLiteFile};Version=3;");
        }
    }
}