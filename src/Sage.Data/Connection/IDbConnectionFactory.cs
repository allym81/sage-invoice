﻿using System.Data;

namespace Sage.Data.Connection
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}
