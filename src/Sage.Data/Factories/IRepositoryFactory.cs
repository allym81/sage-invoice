﻿using Sage.Entites;

namespace Sage.Data.Factories
{
    public interface IRepositoryFactory
    {
        IRepositoryReaderWritter<Invoice> GetInvoiceRepository();

        IRepositoryReader<Customer> GetCustomerRepository();
    }
}
