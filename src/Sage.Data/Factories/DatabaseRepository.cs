﻿using Sage.Data.Connection;
using Sage.Data.Providers;
using Sage.Entites;

namespace Sage.Data.Factories
{
    public class DatabaseRepository: IRepositoryFactory
    {
        private readonly IDbConnectionFactory _connectionFactory;

        public DatabaseRepository(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public IRepositoryReaderWritter<Invoice> GetInvoiceRepository()
        {
            return new InvoiceDatabaseRepository(_connectionFactory);
        }

        public IRepositoryReader<Customer> GetCustomerRepository()
        {
            return new CustomerDatabaseRepository(_connectionFactory);
        }
    }
}