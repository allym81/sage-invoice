using Sage.Data.Adapters;

namespace Sage.Data.Factories
{
    public interface IFileStorageFactory
    {
        IFileStorage GetInstance(string type);
    }
}