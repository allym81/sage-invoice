﻿using System.Collections.Generic;
using Sage.Common;
using Sage.Data.Connection;

namespace Sage.Data.Factories
{
    //NOTE: No need for this. Can easly be done from the DI container at runtime
    //Did this just for Poc
    public class RepositoryLocator : IRepositoryLocator
    {
        private readonly ISageConfiguration _sageConfiguration;

        private readonly IDictionary<string, IRepositoryFactory> _repositoryFactories =
            new Dictionary<string, IRepositoryFactory>();
        public RepositoryLocator(IFileStorageFactory fileStorageFactory, 
            IDbConnectionFactory dbConnectionFactory, 
            ISageConfiguration sageConfiguration)
        {
            _sageConfiguration = sageConfiguration;
            _repositoryFactories.Add("database", new DatabaseRepository(dbConnectionFactory));
            _repositoryFactories.Add("json", new JsonRepositoryFactory(fileStorageFactory));
        } 

        public IRepositoryFactory GetRepositoryFactory()
        {
            IRepositoryFactory repositoryFactory;
            var storageType = _sageConfiguration.GetConfigValue("storageType");
            return _repositoryFactories.TryGetValue(storageType, out repositoryFactory) ? repositoryFactory : null;
        }
    }
}