﻿using System.Collections.Generic;
using System.IO;
using Sage.Common;
using Sage.Data.Adapters;
using Sage.Data.Providers;

namespace Sage.Data.Factories
{
    public class FileStorageFactory : IFileStorageFactory
    {
        private readonly ISageConfiguration _sageConfiguration;
        //This can also be configurable or injected
        private readonly IDictionary<string, string> _filesMappings = new Dictionary<string, string>
        {
            {"customers", "Customers.json" },
            {"invoices",  "Invoices.json"}
        };

        public FileStorageFactory(ISageConfiguration sageConfiguration)
        {
            _sageConfiguration = sageConfiguration;
        }

        public IFileStorage GetInstance(string storageTable)
        {
            string fileName;
            if(_filesMappings.TryGetValue(storageTable, out fileName))
            {
                var path = Path.Combine(_sageConfiguration.GetConfigValue("fileStorageLocation"), fileName);
                return new FileStorage(path);
            }
            return null;
        }
    }
}