﻿using Sage.Data.Providers;
using Sage.Entites;

namespace Sage.Data.Factories
{
    public class JsonRepositoryFactory : IRepositoryFactory
    {
        private readonly IFileStorageFactory _fileStorageFactory;

        public JsonRepositoryFactory(IFileStorageFactory fileStorageFactory)
        {
            _fileStorageFactory = fileStorageFactory;
        }
        public IRepositoryReaderWritter<Invoice> GetInvoiceRepository()
        {
            return new InvoiceJsonRepository(_fileStorageFactory);
        }

        public IRepositoryReader<Customer> GetCustomerRepository()
        {
            return new CustomerJsonRepository(_fileStorageFactory);
        }
    }
}