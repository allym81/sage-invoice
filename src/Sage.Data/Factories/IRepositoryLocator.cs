﻿namespace Sage.Data.Factories
{
    public interface IRepositoryLocator
    {
        IRepositoryFactory GetRepositoryFactory();
    }
}