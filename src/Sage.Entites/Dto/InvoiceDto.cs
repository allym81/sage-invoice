using System;

namespace Sage.Entites.Dto
{
    public sealed class InvoiceDto
    {
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }
}