﻿namespace Sage.Entites.Dto
{
    public sealed class InvoiceStatusDto
    {
        public int Number { get; set; }
        public InvoiceStatus Status { get; set; }
    }
}