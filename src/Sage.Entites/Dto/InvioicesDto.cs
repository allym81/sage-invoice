using System.Collections.Generic;

namespace Sage.Entites.Dto
{
    public sealed class InvioicesDto
    {
        public IEnumerable<Invoice> Invoices { get; set; }
        public decimal Total { get; set; }
    }
}