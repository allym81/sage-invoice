﻿using System;

namespace Sage.Entites
{
    public enum InvoiceStatus
    {
        Pending = 1,
        Paid    
    }

    public sealed class Invoice
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public InvoiceStatus Status { get; set; }
    }
}
