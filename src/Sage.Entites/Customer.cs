﻿namespace Sage.Entites
{
    public sealed class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}