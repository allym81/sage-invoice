﻿using System.Collections.Generic;
using Sage.Entites;
using Sage.Entites.Dto;

namespace Sage.Service
{
    public interface IInvoiceService
    {
        IEnumerable<Invoice> GetInvoiceList();
        InvioicesDto GetInvoiceListWithTotal();
        Invoice GetInvoiceByNumber(int invoiceNumber);
        void CreateInvoice(InvoiceDto invoice);
        bool UpdateInvoiceStatus(InvoiceStatusDto newInvoiceStatus);
    }
}