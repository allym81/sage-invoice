﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sage.Data;
using Sage.Data.Factories;
using Sage.Entites;
using Sage.Entites.Dto;

namespace Sage.Service
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IRepositoryReaderWritter<Invoice> _invoiceRepository;
        private readonly IRepositoryReader<Customer> _customerRepository;

        public InvoiceService(IRepositoryLocator repositoryLocator)
        {
             var repositoryFactory = repositoryLocator.GetRepositoryFactory();
            _invoiceRepository = repositoryFactory.GetInvoiceRepository();
            _customerRepository = repositoryFactory.GetCustomerRepository();
            //TODO: make some assertions - throw if the config is not good
        }

        public InvioicesDto GetInvoiceListWithTotal()
        {
            var invoices = _invoiceRepository.GetAll().ToList();
            var total = invoices.Sum(x => x.Amount);
            return new InvioicesDto
            {
                Invoices = invoices,
                Total = total
            };
        }

        public void CreateInvoice(InvoiceDto invoice)
        {
            var rnd = new Random();
            var customer = _customerRepository.Get(invoice.CustomerId);
            if (customer == null)
            {
                throw new ArgumentException("Invalid customer specified.");
            }
            var newInvoice = new Invoice
            {
                Number = rnd.Next(1, int.MaxValue),
                Description = invoice.Description,
                CustomerId = invoice.CustomerId,
                Amount = invoice.Amount,
                Date = invoice.Date,
                Status = InvoiceStatus.Pending
            };

            _invoiceRepository.Insert(newInvoice); 
        }

        public IEnumerable<Invoice> GetInvoiceList()
        {
            return _invoiceRepository.GetAll();
        }

        public Invoice GetInvoiceByNumber(int invoiceNumber)
        {
            return _invoiceRepository.Get(invoiceNumber);
        }

        public bool UpdateInvoiceStatus(InvoiceStatusDto newInvoiceStatus)
        {
            var invoiceToUpdate = _invoiceRepository.Get(newInvoiceStatus.Number);
            if (invoiceToUpdate == null)
            {
                //todo log this;
                return false;
            }
            if (newInvoiceStatus.Status < invoiceToUpdate.Status) return false; //do not allow to go back from paid to pending
            invoiceToUpdate.Status = newInvoiceStatus.Status;
            _invoiceRepository.Update(invoiceToUpdate);
            return true;
        }
    }
}
