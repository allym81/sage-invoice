﻿using System;
using System.Linq;
using Nancy;
using Nancy.ModelBinding;
using Sage.Data;
using Sage.Entites;
using Sage.Entites.Dto;
using Sage.Service;

namespace Sage.Api
{
    public class InvoiceModule : NancyModule
    {
        private readonly IInvoiceService _invoiceService;

        public InvoiceModule(IInvoiceService invoiceService) : base("/invoice")
        {
            _invoiceService = invoiceService;
            Get["/"] = _ => GetAllInvoices();
            Get["/{invoiceNumber}"] = _ => GetInvoice(_.invoiceNumber);
            Put["/"] = _ => CreateNewInvoice();
            //usually when using patch the payload should contain the path of the obj that is updated. 
            //For dev speed i've put it in the uri path
            Patch["/status"] = _ => UpdateInvoiceStatus();
        }

        private object UpdateInvoiceStatus()
        {
            var newInvoiceStatus = this.Bind<InvoiceStatusDto>();
            var result = _invoiceService.UpdateInvoiceStatus(newInvoiceStatus);
            return !result ? HttpStatusCode.BadRequest : HttpStatusCode.Accepted;
        }

        private object CreateNewInvoice()
        {
            var newInvoice = this.Bind<InvoiceDto>();
            _invoiceService.CreateInvoice(newInvoice);
            return HttpStatusCode.Created;
        }

        private object GetInvoice(int invoiceNumber)
        {
            var invoice = _invoiceService.GetInvoiceByNumber(invoiceNumber);
            if (invoice == null) return HttpStatusCode.NotFound;
            return invoice;
        }

        private object GetAllInvoices()
        {
            return _invoiceService.GetInvoiceListWithTotal();
        }
    }
}