﻿using System.Configuration;
using Nancy;
using Nancy.Conventions;
using Nancy.TinyIoc;
using Sage.Common;
using Sage.Data;
using Sage.Data.Adapters;
using Sage.Data.Connection;
using Sage.Data.Factories;
using Sage.Data.Providers;
using Sage.Entites;
using Sage.Service;

namespace Sage.Api
{
    public class SageBootstraper : DefaultNancyBootstrapper
    {
        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);
            container.Register<ISageConfiguration, SageConfiguration>().AsSingleton();
            container.Register<IFileStorageFactory, FileStorageFactory>().AsSingleton();
            container.Register<IDbConnectionFactory, DbConnectionFactory>().AsSingleton();
        }

        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
        {
            CreateRequestContainer(context);
            container.Register<IRepositoryLocator, RepositoryLocator>().AsSingleton();
            container.Register<IInvoiceService, InvoiceService>().AsSingleton();
        }
    }
}