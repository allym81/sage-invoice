﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Sage.Data;
using Sage.Data.Factories;
using Sage.Entites;

namespace Sage.Api
{
    public class CustomerModule : NancyModule
    {
        private readonly IRepositoryLocator _repositoryLocator;
        public CustomerModule(IRepositoryLocator repositoryLocator) : base("/customer")
        {
            _repositoryLocator = repositoryLocator;
            Get["/"] = _ => GetAllCustomers();
        }

        private object GetAllCustomers()
        {
            //todo check valid config, null, etc
            var customerRepository = _repositoryLocator.GetRepositoryFactory().GetCustomerRepository();
            return customerRepository.GetAll();
        }
    }
}
