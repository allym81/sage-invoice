﻿namespace Sage.Common
{
    public interface ISageConfiguration
    {
        string GetConfigValue(string key);
    }
}