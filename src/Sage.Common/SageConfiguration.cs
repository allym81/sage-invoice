﻿using System.Configuration;

namespace Sage.Common
{
    public class SageConfiguration : ISageConfiguration
    {
        public string GetConfigValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
