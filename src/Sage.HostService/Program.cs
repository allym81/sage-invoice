﻿using System;
using System.Configuration;
using Nancy.Hosting.Self;

namespace Sage.HostService
{
    class Program
    {
        static void Main(string[] args)
        {
            var port = ConfigurationManager.AppSettings["port"];
            if (string.IsNullOrEmpty(port))
            {
                port = "1234";
            }
            var url = "http://localhost:" + port;
            using (var host= new NancyHost(new Uri(url)))
            {
                host.Start();
                Console.WriteLine(url);
                Console.ReadLine();
            }
        }
    }
}
