'use strict';

angular.module('sageApp', ['ngRoute', 'ui.grid', 'ui.bootstrap'])
    .constant('InvoiceStatuses',
    [{id: 1, name: 'Pending'},
    {id: 2, name: 'Paid'}
    ])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'main/main.html',
            controller: 'MainController',
            controllerAs: 'vm'
        });
    }])

    .controller('MainController', ['InvoiceService', 'CustomerService', '$uibModal', 'InvoiceStatuses',
        function (InvoiceService, CustomerService, $uibModal, InvoiceStatuses) {
            var vm = this;
            vm.isDisplayedAddNewForm = false;
            vm.gridOptions = {
                enableColumnResizing: true,
                enableRowSelection: false,
                columnDefs: [
                    {
                        field: 'id',
                        displayName: 'Id',
                        enableCellEdit: false,
                        visible: false
                    },
                    {
                        field: 'number',
                        displayName: 'Number',
                        enableCellEdit: false
                    }, {
                        field: 'date',
                        displayName: 'Date',
                        enableCellEdit: false,
                        cellFilter: 'date'
                    },
                    {
                        field: 'customer',
                        displayName: 'Customer',
                        enableCellEdit: false,
                        cellTemplate: '<span ng-bind="row.entity.customer.name"></span>'
                    }, {
                        field: 'description',
                        displayName: 'Description',
                        enableCellEdit: false
                    },
                    {
                        field: 'amount',
                        displayName: 'Amount',
                        enableCellEdit: true,
                        editableCellTemplate: 'ui-grid/dropdownEditor'
                    },
                    {
                        field: 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellFilter: 'invoiceStatusFilter'
                    },
                    {
                        field: 'id',
                        displayName: '',
                        cellTemplate: '<button type="button" class="btn btn-xs btn-primary" ' +
                                    'ng-click="grid.appScope.vm.editInvoiceStatus(row.entity)"><i class="fa fa-edit"></i></button>',
                        width: 45
                    }]
            };
            vm.total = 0;
            vm.invoices = [];
            vm.customers = [];

            this.editInvoiceStatus = function(row) {
                var selectedInvoice = row;
                var dialog = $uibModal.open({
                    templateUrl: 'views/editInvoice.html',
                    controller: ['$uibModalInstance', 'InvoiceStatuses', 'model',
                        function ($uibModalInstance, InvoiceStatuses, model) {
                            this.invoiceNumber = model.number;
                            this.invoiceStatus = model.status;
                            this.invoiceStatuses = InvoiceStatuses;
                            this.save = function () {
                                $uibModalInstance.close(this.invoiceStatus);
                            };
                        }],
                    controllerAs: 'vm',
                    resolve: {
                        model: function () {
                            return selectedInvoice;
                        }
                    }
                });
                dialog.result.then(function (result) {
                    if (result) {
                        var updatedInvoiceStatus = {
                            number: selectedInvoice.number,
                            status: result
                        };
                        InvoiceService.updateInvoiceStatus(updatedInvoiceStatus)
                            .then(function(result) {
                                if (result) {
                                    loadInvoices();
                                } else {
                                    alert('Error updating the invoice.')
                                }
                            });
                    }
                });
            };
            this.toogleAdd = function () {
                if (vm.customers.length > 0) {
                    vm.isDisplayedAddNewForm = true;
                    vm.newInvoice = {
                        date: new Date(),
                        customerId: -1,
                        description: '',
                        amount: 0
                    };
                } else {
                    alert('No customers. Cannot create new invoice without customers.')
                }
            };

            this.createInvoice = function () {
                InvoiceService.createInvoice(vm.newInvoice).then(function (result) {
                    if (result) {
                        loadInvoices();
                    } else {
                        alert('Error creating invoice.');
                    }
                });
            };

            function loadInvoices() {
                InvoiceService.getInvoices()
                    .then(function (result) {
                        vm.gridOptions.data = result.invoices;
                        vm.total = result.total;
                    });
            };

            function loadCustomers() {
                CustomerService.getCustomers()
                    .then(function (result) {
                        if (result && result.length > 0) {
                            vm.customers = result;
                        }
                    });
            };

            loadInvoices();
            loadCustomers();
        }]).filter('invoiceStatusFilter', ['InvoiceStatuses', function (InvoiceStatuses) {
            return function (value) {
                var status = InvoiceStatuses.filter(function (v) {
                    return v.id === value;
                })[0];
                return status.name
            }
        }]);