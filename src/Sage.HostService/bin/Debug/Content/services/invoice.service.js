angular
    .module('sageApp')
    .service('InvoiceService', ['$http', function($http) {
        this.getInvoices = function () {
            return $http.get('/invoice').then(function (result) {
                return result.data;
            }, function (error) {
                return [];
            })
        };
        this.createInvoice = function(invoice) {
            return $http.put('/invoice', invoice).then(function (result) {
                return true;
            }, function (err) {
               return false;
            });
        };
        this.updateInvoiceStatus = function(updatedInvoiceStatus) {
            return $http.patch('/invoice/status', updatedInvoiceStatus)
                .then(function (result) {
                    return true;
                }, function (err) {
                    return false;
                });
        };
    }]);
